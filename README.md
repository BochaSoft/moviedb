MOVIEDB es una aplicación que sirve para llevar un registro de las peliculas vistas o no vistas.

Cuenta con 2 bases de datos ClouDB y TinyDB, se utilizo Cloudb para realizar la lista de catálogos y colocarlos en sagas.
Dentro de las sagas es posible ver las peliculas que la componen con una miniatura y al dar "click largo" estas se tildaran como vista o no vista.

Para el desarrollo de la mecánica se utilizo TinyDB para guardar cada una de las variables globales y asi poder independizar cada portada con su respectivo resultado de tildes,
y al momento, tanco como volver a la pantalla anterior o cerrar y volver a abrir la APP que los resultados permanezcan en la misma manera que fueron dejados.
Gracias a este método es posible dejar registro de las películas que vallan viendo.

